FROM node:10

COPY . /code
WORKDIR /code

RUN npm install

CMD ["npx", "nodemon", "server.js"]

